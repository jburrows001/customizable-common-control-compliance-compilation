# Converting the csv to a dataframe

import pandas as pd

df = pd.read_csv('./controls_csv.csv')

## Source:

* https://chrisalbon.com/python/data_wrangling/pandas_dataframe_importing_csv/

# Outputing the dataframe to json

df.to_json(r'./controls.json')

## Result

The output treats each row as a block of json instead of treating each column as a block. The desired result would look like this:

```
"name": "GitLab Security Compliance Controls - Phase 1",
"children": [
  {
    "name": "Backup Management",
    "children": [
      {
        "name": "Backup",
        "children": [
          {
            "name": "Backup Configuration",
            "control_number": "BU.1.01",
            "control_family": "Backup Management",
            "control_sub-family": "Backup",
            "guidance_link": "",
            "control_statement": "[The organization] configures redundant systems or performs data backups [in accordance with the organization-defined frequency] to resume system operations in the event of a system failure.",
            "iso_map" : ["A.18.1.3"],
            "soc-cc_map" : [""],
            "soc-a_map" : [""],
            "fedramp_map" : [""],
            "pci_map" : ["12.10.1"],
            "glba_map" : [""],
            "ferpa_map" : [""],
            "sox_map" : [""],
            "raci_responsible": [""],
            "raci_accountable": [""],
            "raci_consulted": [""],
            "raci_informed": [""]
          },
          {
            "name": "Resilience Testing",
            "control_number": "BU.1.02",
            "control_family": "Backup Management",
            "control_sub-family": "Backup",
            "guidance_link": "",
            "control_statement": "[The organization] performs backup restoration or failover tests [in accordance with the organization-defined frequency] to confirm the reliability and integrity of system backups or recovery operations.",
            "iso_map": ["A.12.3.1"],
            "soc-cc_map" : [""],
            "soc-a_map" : [""],
            "fedramp_map" : [""],
            "pci_map" : ["12.10.1"],
            "glba_map" : [""],
            "ferpa_map" : [""],
            "sox_map" : [""],
            "raci_responsible": [""],
            "raci_accountable": [""],
            "raci_consulted": [""],
            "raci_informed": [""]
          }
        ]
      }
    ]
  },
  ```
