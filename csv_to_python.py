import pandas as pd

df = pd.read_csv('./controls_csv.csv')

# The below output treats each row as a block of json instead of treating each column as a block
df.to_json(r'./controls.json')
