# Customizable Common Control Compliance Compilation

This project extends the [Adobe open source compliance program](https://blogs.adobe.com/security/2017/05/open-source-ccf.html). The goal in extending this is to make the customization and deployment of this program easier for small companies starting to build out their compliance programs.

## CCF overview

The purpose of this compliance framework is to collect the requirements for the most common compliance frameworks and create control statements in a way that naturally satisfies all underlying framework requirements. The benefit of this approach is that instead of going to a team with a few hundred requirements from the 5 compliance regulations/frameworks your company might be subject to, you only need to go to your teams with 1 control per topic. The ultimate goal with this approach is efficiency.

## Current State

I have a csv file with the data I want to use, but I don't want to keep this information in the csv format. I believe the right way to store this data is an individual yaml file per control (or "row" in the csv). I'm currently working to convert the information in the [controls csv file](./controls_csv.csv) to individual yaml files (1 file for each row).

## Success Criteria

1. Create a repo that a company can clone and by running the scripts located in that repo, have a full set of security controls customized to their organization
2. Create enough documentation that a company with no compliance SME's would be able to define their information security controls and supporting documentation
3. Create a project that a company could clone that would walk them through the steps needed to perform a gap analysis

## Next Steps

1. Create a python script that outputs 1 yaml file per row in the [controls csv file](./controls_csv.csv) with a data type per column.
2. Take all data from the yaml files and create a variable file that allows you to define each generic term in 1 central location
3. Write a shell script that will take the variable file and replace all related yaml files at once
4. Find a way to display controls based on the frameworks that are relevant to a specific company (e.g. if a company is only interested in SOC2 common controls and ISO 27001, they can display controls that only have non-null data in the related `soc-cc_map` and `iso_27001_map` fields)
5. Create a gap analysis template project that companies can clone to have the first steps needed to performing a gap analysis
